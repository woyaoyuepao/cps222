#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BUFSIZE 128

int main(int argc, char *argv[])
{
	printf("Enter a number: ");
	char digit[BUFSIZE];
	int length;
	char c;
	fgets(digit, sizeof(digit),stdin);
    length = strlen(digit);
	digit[length] = '\0';
	int occurrences[9] = {0};
	printf("Digit:          ");
	for(int i = 0; i < 10; i++) {
		printf("%d ", i);
	}	
	printf("\n");
	printf("Occurrences:    ");
    
    for (int i = 0; i < length; i++){
    	c = digit[i];
    	occurrences[c - 48]++;
    }

    for (int i = 0; i < 10; i++) {
    	printf("%d ", occurrences[i]);
    }
    printf("\n");
    return 0;
}