# 4_2 逆序打印三位数

## 题目

扩展第 4 章编程题 1 使其可以处理 3 位数。

## 样例

### 样例一

      Enter a three-digit number: 123
      The reversal is: 321

### 样例二

      Enter a three-digit number: 915
      The reversal is: 519

## 数据范围

可以假设输入的数为 `n` 范围为，则 100 ≤ n ≤ 999 或 -999 ≤ n ≤ -100 。

## 提示

1. 请勿遗漏负数的情况

2. 按照题目内容中的输出为规范格式进行输出
