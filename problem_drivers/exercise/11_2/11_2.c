#include<stdio.h>
#include<math.h>
#include<stdlib.h>
void find_closest_flight(int desired_time,int *departure_time,int *arrival_time)
{  
   int a[] = { 480,583,679,767,840,945,1140,1305 };
   int i;
   int choice = 1305;
	for (i = 0; i <= 7; i++)
	{
		choice = (abs(desired_time - choice) > abs(desired_time - a[i]) ? a[i] : choice);
	}
	switch (choice) {
	case 480:
		*departure_time = 480 ;*arrival_time = 616 ;
		//printf("Closest departure time is 8:00 a.m., arriving at 10:16 a.m.");
		break;
	case 583:
	    *departure_time = 583 ;*arrival_time = 712;
		//printf("Closest departure time is 9:43 a.m., arriving at 11:52 a.m.");
		break;
	case 679:
	    *departure_time = 679 ;*arrival_time = 811;
		//printf("Closest departure time is 11:19 a.m., arriving at 1:31 p.m.");
		break;
	case 767:
	    *departure_time = 767 ;*arrival_time = 900;
		//printf("Closest departure time is 12:47 p.m., arriving at 3:00 p.m.");
		break;
	case 840:
	    *departure_time = 840 ;*arrival_time = 968;
		//printf("Closest departure time is 2:00 p.m., arriving at 4:08 p.m.");
		break;
	case 945:
	    *departure_time = 945 ;*arrival_time = 1075;
		//printf("Closest departure time is 3:45 p.m., arriving at 5:55 p.m.");
		break;
	case 1140:
	    *departure_time = 1140 ;*arrival_time = 1280;
		//printf("Closest departure time is 7:00 p.m., arriving at 9:20 p.m.");
		break;
	case 1305:
	    *departure_time = 1305 ;*arrival_time = 1438;
		//printf("Closest departure time is 9:45 p.m., arriving at 11:58 p.m.");
		break;
	}
	
}
int main()
{
	int  ip_time1, ip_time2, cl_time;
	int departure_time,arrival_time;
	printf("Enter a 24-hour time:");
	scanf("%d:%d", &ip_time1, &ip_time2);
	cl_time = ip_time1 * 60 + ip_time2;
    find_closest_flight(cl_time,&departure_time,&arrival_time);

    if( departure_time/60 == 12)
    printf("Closest departure time is 12:%2d p.m.",(departure_time%60));
	else if(departure_time <= 720 && departure_time/60 != 12)
    printf("Closest departure time is %2d:%02d a.m.",(departure_time/60),(departure_time%60));
    else
    printf("Closest departure time is %2d:%02d p.m.",(departure_time/60 - 12),(departure_time%60));    
    if(arrival_time <= 720)
    printf(", arriving at %2d:%02d a.m.",(arrival_time/60),(arrival_time%60));
    else
    printf(", arriving at %2d:%02d p.m.",(arrival_time/60 - 12),(arrival_time%60)); 
    printf("\n");
   	return 0;
}