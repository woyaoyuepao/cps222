#!/bin/bash

# Variable definition
std_filename='std'
stu_filename='stu'
std_output='std.txt'
stu_output='stu.txt'
case_file="case_file"

# Compile source file
echo -e "Compiling ${std_filename}...\c"
gcc $std_filename.c -o $std_filename.o
status=$?
# Exit if something wrong
if [ $status -ne 0 ]
then
    echo -e "\nFATA: cannot compile ${std_filename}.c (returned $status)"
    exit 1
fi
echo "Done"

echo -e "Compiling ${stu_filename}...\c"
gcc $stu_filename.c -o $stu_filename.o
status=$?
if [ $status -ne 0 ]
then
    echo -e "\nFATA: cannot compile ${stu_filename}.c (returned $status)"
    exit 1
fi
echo "Done"

# Get standard soulution if necessary
./makeresult.sh $std_filename.o $case_file > $std_output

# Erase student output file
> $stu_output

# Get student output file
while read line
do
    echo ${line}|./$stu_filename.o >> $stu_output
    echo -e "" >> $stu_output
done < $case_file
# Check output files and print if different
echo "Checking output file..."
diff -u $std_output $stu_output
status=$?
if [ $status -eq 0 ]
then
    echo "diff nowhere."
fi
# run grading program
java -classpath commons-io-2.6.jar:. AutoGrading $std_filename.c $std_output $stu_filename.c $stu_output
