#include<stdio.h> 
#include<ctype.h> 
int main()
{
	int ch;
	int line = 0;
	int flag = 1;
	while ((ch = getchar()) != EOF)
	{
		if (ch < 65 || ch > 122 )
		{
			flag = 1;
		}
		if (ch >= 65 && ch <= 122 && flag == 1)
		{
			flag = 2;

		}
		if (flag == 1)
		{
			putchar(ch);
			continue;
		}
		if (flag == 2)
		{
			putchar(toupper(ch));
			flag = 0;
			continue;
		}
		if (flag == 0)
		{
			putchar(ch);
			continue;
		}
		
	}

	return 0;
}