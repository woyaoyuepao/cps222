# 7_13 求句子平均词长

## 题目

编写程序计算句子的平均词长：

    Enter a sentence: It was deja vu all over again.
    Average word length: 3.4

简单起见，程序中把标点符号看作其前面单词的一部分。平均词长显示一个小数位。

## 样例

### 样例一

    Enter a sentence: It was deja vu all over again.
    Average word length: 3.4

### 样例二

    Enter a sentence: Whatever is worth doing is worth doing well.
    Average word length: 4.6

## 数据范围

1. 输出的平均词长应为float类型，保留1位小数

## 提示

1. 按照题目内容中的输出为规范格式进行输出。