/*********************************************************
 * From C PROGRAMMING: A MODERN APPROACH, Second Edition *
 * By K. N. King                                         *
 * Copyright (c) 2008, 1996 W. W. Norton & Company, Inc. *
 * All rights reserved.                                  *
 * This program may be freely distributed for class use, *
 * provided that this copyright notice is retained.      *
 *********************************************************/

/* canopen.c (Chapter 22, page 547) */
/* Checks whether a file can be opened for reading */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  FILE *fp;
  int count = 0;

  if (argc == 1) {
    printf("Please input the filename.\n");
    exit(EXIT_FAILURE);
  }
  int i; 
  for (i = 1; i < argc; ++i) {
      if ((fp = fopen(argv[i], "r")) == NULL) {
          printf("%s can't be opened\n", argv[i]);
		  count++;
      }
      else {
          printf("%s can be opended\n", argv[i]);
          fclose(fp);
      }

  }
  if (count > 0)
	  exit(EXIT_FAILURE);
  return 0;
}
