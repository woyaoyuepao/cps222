# 22_1 判断文件是否可以打开 Judge file

## 题目

扩展 22.2 节的 canopen.c 程序，以便用户可以把任意数量的文件名放置在命令行中。这个程序
应该为每个文件分别显示 can be opended 消息或者 can't be opened 消息。如果一个或多个文
件无法打开，程序以 EXIT_FAILURE 状态终止。

## 样例

### 样例一

输入：

    canopen foo bar baz

输出：

    foo can be opended
    bar can be opended
    baz can be opended

### 样例二

输入：

    canopen foo hello bar

输出：

    foo can can be opended
    hello can't be opened
    bar can be opended

## 数据范围

命令行中的命令行参数可以为文件名或者是任意字符串。如果是可读的文件则可以打开，如果是任意
字符串则认为该文件不可读则无法打开。
