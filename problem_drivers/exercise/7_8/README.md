# 7_8 修改5_8 Time Format：

## 题目

修改第 5 章的编程题 8，要求用户输入 12 小时制的时间。输入时间的格式为 “时：分”，后跟 A、P、AM 或 PM（大小写均可）。数值时间和 AM/PM 之间允许有空白（但不强制要求有空白）。有效输入的示例如下：

    1:15P
    1:15PM
    1:15p
    1:15pm
    1:15 P
    1:15 PM
    1:15 p
    1:15 pm

## 样例

### 样例一

    Enter a 12-hour time: 2:34am
    Closest departure time is 8:00 a.m., arriving at 10:16 a.m.

### 样例二

    Enter a 12-hour time: 2:34 a
    Closest departure time is 8:00 a.m., arriving at 10:16 a.m.

## 数据范围

假定输入的格式就是上述之一，并且不需要出错判定。