# 22_16 复制文件 Copy file

## 题目

修改 22.4 节的 fcopy.c 程序，使其用 fread 和 fwrite 来复制文件，复制时使用 512 字节的块（当然，最后一个块包含的字节数可能少于 512）。

## 样例

### 样例一

输入：

    copy sourcefile destfile

从命令行输入，其中 sourcefile 是源文件名， destfile 是目标文件名。
