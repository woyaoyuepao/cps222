# 21_1 输出空洞 Print offset

## 题目

编写一个程序声明结构 s（见 21.4 节），并显示出成员 a、b、c 的大小和偏移量。（使用 sizeof 来得到大小，使用 offset 来得到偏移量。）同时使程序显示出整个结构的大小。根据这些信息，判断结构中是否包含空洞。如果包含空洞，输出总的空洞大小。

## 样例

### 样例一

    the size of a: 1,  the offset of a：0
    the size of b: 8,  the offset of b：4
    the size of c: 4,  the offset of c：12
    the total empty hole:3

## 提示

因编译器差异，所以不必再输出位置，只需要输出总的空洞大小即可。