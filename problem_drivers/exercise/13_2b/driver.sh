#!/bin/bash

# Variable definition
std_filename='std'
stu_filename='stu'
std_output='std.txt'
stu_output='stu.txt'

# Compile source file
echo -e "Compiling ${std_filename}...\c"
gcc $std_filename.c -o $std_filename.o
status=$?
# Exit if something wrong
if [ $status -ne 0 ]
then
    echo -e "\nFATA: cannot compile ${std_filename}.c (returned $status)"
    exit 1
fi
echo "Done"

echo -e "Compiling ${stu_filename}...\c"
gcc $stu_filename.c -o $stu_filename.o
status=$?
if [ $status -ne 0 ]
then
    echo -e "\nFATA: cannot compile ${stu_filename}.c (returned $status)"
    exit 1
fi
echo "Done"

# Get standard soulution if necessary
echo -e "24 Susan's birthday\n \
5 6:00 - Dinner with Marge and Russ\n \
23 Movie - "Chinatown"\n \
-1 Saturday Class\n \
32 Sunday Class\n \
0\n"|./$std_filename.o >> $std_output
echo -e "815 Tom's birthday\n \
50 6:00 - Dinner with Marge and Russ\n \
-2 Movie - "pigpeiqi"\n \
-1 	Having party\n \
22 Monday Class\n \
0\n"|./$std_filename.o >> $std_output
# Get student output file
echo -e "24 Susan's birthday\n \
5 6:00 - Dinner with Marge and Russ\n \
23 Movie - "Chinatown"\n \
-1 Saturday Class\n \
32 Sunday Class\n \
0\n"|./$stu_filename.o >> $stu_output
echo -e "815 Tom's birthday\n \
50 6:00 - Dinner with Marge and Russ\n \
-2 Movie - "pigpeiqi"\n \
-1 	Having party\n \
22 Monday Class\n \
0\n"|./$stu_filename.o >> $stu_output
# Check output files and print if different
echo "Checking output file..."
diff -u $std_output $stu_output
status=$?
if [ $status -eq 0 ]
then
    echo "diff nowhere."
fi
# run grading program
java -classpath commons-io-2.6.jar:. AutoGrading $std_filename.c $std_output $stu_filename.c $stu_output
