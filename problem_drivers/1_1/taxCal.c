#include<stdio.h>
#define TAX_RATE 1.05

int main(){
    float amount,result;
    printf("Enter an amount: ");
    scanf("%f",&amount);
    result = amount * TAX_RATE;
    printf("With tax added: $%.2f\n",result); 
    return 0;
}

