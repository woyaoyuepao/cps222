#include<stdio.h>
#include<string.h>
#define MAX_SIZE 50

int main(){
    int c,i;
    char line[MAX_SIZE];
    printf("Enter a message: ");
    for(i = 0; (c = getc(stdin)) != '\n' && i < MAX_SIZE;)
        line[i++] = c;
    /* line[i] = '\0'; */
    printf("Reversal is: ");
    while(i > 0)
        printf("%c",line[--i]);
    printf("\n");
    return 0;
}

