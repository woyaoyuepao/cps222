# 3_6 日期转换 Date conversion

## 题目

修改 3.2 节的 addfrac.c 程序，使用户可以同时输入两个分类，中间用加号隔开：

## 样例

### 样例一

    Enter two fractions separated by a plus sign: 5/6+3/4
    The sum is 38/24

## 数据范围

所输入的日期必须符合正常范畴。
按照题目内容中的输出为规范输出。